# ShinyMatrix

The `heatmap.2` function of the `gplots` package is very powerful yet complicated to use in the R console and configure for normal usage. There are many different settings that can change the way your final plot looks. 

With ShinyMatrix, you can upload and easily visualize a matrix of binary or continuous data using ShinyMatrix's convenient GUI for many of the `heatmap.2` function parameters.

After setting up your plot to your liking, you can export and download the plot as

- SVG
- PNG
- PDF
