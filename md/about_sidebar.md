### Source Code 

The source code is available at:

https://bitbucket.org/peterk87/shiny_matrix

### Issues

If you have any issues, please let us know here:

https://bitbucket.org/peterk87/shiny_matrix/issues?status=new&status=open