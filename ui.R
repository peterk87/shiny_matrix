require('shiny')
require('RColorBrewer')

# The shinyalert and showshinyalert are taken from AnalytixWare/ShinySky by xiaodaigh 
# https://github.com/AnalytixWare/ShinySky/blob/master/R/shiny-alert.R
shiny_alert_container <- function(id) 
{
  tagList(
    tags$head(singleton(tags$script(src = "js/shinyalert.js")))
    ,HTML(paste0('<div id="', id, '" class="shinyalert"></div>'))
  )
}

textInputRow <-function (inputId, label, value=0, type='number', class='input-small', ...) 
{
  div(style="display:inline-block"
    ,tags$label(label
      ,`for`=inputId
    )
    ,tags$input(id=inputId
      ,type=type
      ,value=value
      ,class=class
      ,...
    )
  )
}


get_dataset_paths <- function()
{
  args_list_files <- list(
    path='./data/', 
    pattern='\\.RData$',
    full.names=TRUE
    )
  dataset_paths <- do.call(list.files, args_list_files)
  args_list_files$full.names <- FALSE
  dataset_filenames <- do.call(list.files, args_list_files)
  dataset_filenames <- sub(
    pattern='\\.RData$', 
    replacement='', 
    x=dataset_filenames)
  names(dataset_paths) <- dataset_filenames
  dataset_paths
}

get_ordinal_color_schemes <- function()
{
  df <- brewer.pal.info
  color_schemes <- rownames(df)
  names(color_schemes) <- paste0(color_schemes, 
    ' n=', df$maxcolors, 
    ' type=', df$category)
  color_schemes
}

shinyUI(navbarPage('ShinyMatrix'
  ,tabPanel('App', pageWithSidebar(
    headerPanel('')
    ,sidebarPanel(
      tags$head(
        tags$style(type='text/css', '.span4 { max-width: 360px; }')
        ,tags$style(type='text/css', 'a[disabled] { pointer-events: none; }')
        ,tags$link(href='css/jquery-ui-1.10.4.custom.css', type='text/css', rel='stylesheet')
        ,tags$link(href='css/spectrum.css', type='text/css', rel='stylesheet')
        ,tags$script(src='js/spectrum.js')
        ,tags$script(src='js/shiny_disable_control.js')
      )
      # enable Shiny's jQueryUI with this little hack:
      ,absolutePanel(draggable=TRUE)

      ,div(class='well well-small'
        ,h4('Upload a matrix to visualize')
        ,selectizeInput('filetype_upload_matrix'
          ,'File type of uploaded file'
          ,choices=c(
            'Tab-separated values (TSV)'='tsv'
            ,'Comma-separated values (CSV)'='csv'
          )
          ,width='100%'
        )
        ,fileInput('file_upload_matrix', 'Upload matrix')
        ,shiny_alert_container('matrix_file_ul_status')
        ,div(id='matrix_control_panel', style='display:none;'
          ,checkboxInput('is_binary_matrix'
            ,'Binary matrix?'
            ,value=TRUE
          )
          ,checkboxInput('do_hc_row'
            ,'Perform hierarchical clustering of rows'
            ,value=FALSE
          )
          ,checkboxInput('do_hc_col'
            ,'Perform hierarchical clustering of columns'
            ,value=FALSE
          )
          # if hierarchical clustering for either the rows or columns is to be
          # performed then show options for changing the distance and hierarchical
          # clustering methods
          ,conditionalPanel(condition='input.do_hc_col || input.do_hc_row'
            ,selectizeInput('dist_method'
              ,'Distance method'
              ,choices=c(
                'Pairwise % Distance'='pairwise'
                ,Euclidean="euclidean"
                ,Maximum="maximum"
                ,Manhattan="manhattan"
                ,Canberra="canberra"
                ,Binary="binary"
                ,Minkowski="minkowski"
              )
              ,selected='pairwise'
              ,width='100%'
            )
            ,selectizeInput('hc_method'
              ,'Hierarchical Clustering method'
              ,choices=c(
                'Complete linkage'='complete'
                ,'Average linkage (UPGMA)'='average'
                ,'Single linkage'='single'
                ,'Median linkage (WPGMC)'='median'
                ,'Centroid linkage (UPGMC)'='centroid'
                ,'Ward\'s minimum variance'='ward.D2'
              )
              ,selected='complete'
              ,width='100%'
            )
          )
          ,checkboxInput('transposed'
            ,'Transpose matrix?'
            ,value=FALSE
          )
          ,checkboxInput('show_heatmap_key'
            ,'Show key in plot?'
            ,value=FALSE
          )
        )
      )
      ,br()
      ,div(id='dialog_load_demo_dataset', class='dialog', title='Load a demo dataset'
        ,shiny_alert_container('load_dataset_alert')
        ,selectizeInput('dataset'
          ,'Select Dataset'
          ,choices=c(None='', get_dataset_paths())
          ,multiple=FALSE
          ,width='100%'
        )
        ,actionButton('action_load_dataset'
          ,'Load Dataset'
          ,icon=icon('play'))
      )
      ,div(id='dialog_plot_dim', class='dialog', title='Change Plot Parameters'
        ,h5('Plot Dimensions')
        ,textInputRow('height_matrix_viz'
          ,'Height (px)'
          ,value=600
          ,step=10
        )
        ,textInputRow('width_matrix_viz'
          ,'Width (px)'
          ,value=800
          ,step=10
        )
        ,br()
        ,h5('Row and Column Names')
        ,checkboxInput('show_row_labels'
          ,'Show row labels'
          ,value=TRUE)
        ,conditionalPanel(condition='input.show_row_labels'
          ,textInputRow('row_margin_matrix_viz'
            ,'Row Label Margin'
            ,value=5
            ,step=1
            ,class='input-medium'
          )
          ,textInputRow('row_label_size'
            ,'Row Label Size'
            ,value=1
            ,min=0.1
            ,step=0.1
            ,class='input-medium'
          )
        )
        ,checkboxInput('show_col_labels'
          ,'Show column labels'
          ,value=TRUE)
        ,conditionalPanel(condition='input.show_col_labels'
          ,textInputRow('col_margin_matrix_viz'
            ,'Column Label Margin'
            ,value=5
            ,step=1
            ,class='input-medium'
          )
          ,textInputRow('col_label_size'
            ,'Column Label Size'
            ,value=1
            ,min=0.1
            ,step=0.1
            ,class='input-medium'
          )
          ,br()
          ,textInputRow('col_label_angle'
            ,'Column Label Angle (degrees)'
            ,value=45
            ,min=0
            ,max=360
            ,step=5
          )
        )
        ,br()
        ,h5('Matrix colours')
        ,conditionalPanel(condition='input.is_binary_matrix === true'
          ,div(style="display:inline-block"
            ,textInput('color_absent'
              ,'Color for absence (0)'
              ,value='gray'
            )
            ,textInput('color_present'
              ,'Color for presence (1)'
              ,value='black'
            )
            ,tags$script('
              $("#color_absent").spectrum();
              $("#color_present").spectrum();
              '
            )
          )
        )
        ,conditionalPanel(condition='input.is_binary_matrix === false'
          ,selectizeInput('ordinal_colour_scheme'
            ,'Ordinal colour scheme'
            ,choices=get_ordinal_color_schemes()
            ,multiple=FALSE
          )
        )
        ,conditionalPanel(condition='input.do_hc_row'
          ,checkboxInput('show_row_dendrogram'
            ,'Show row dendrogram'
            ,value=TRUE)
        )
        ,conditionalPanel(condition='input.do_hc_col'
          ,checkboxInput('show_col_dendrogram'
            ,'Show column dendrogram'
            ,value=TRUE)
        )
      )
      ,div(id='dialog_export_plot', class='dialog', title='Export...'
        ,shiny_alert_container('alert_export')
        ,h5('Export Plot')
        ,selectizeInput('filetype_dl_plot'
          ,'Export plot as'
          ,choices=c(
            None=''
            ,PDF='pdf'
            ,SVG='svg'
            ,PNG='png'
            )
          ,width='100%'
          )
        ,conditionalPanel(condition='input.filetype_dl_plot == "pdf" 
          || input.filetype_dl_plot == "svg"'
          ,checkboxInput('dl_plot_rasterize'
            ,'Rasterize matrix plot?'
            ,value=TRUE
          )
        )
        ,downloadButton('dl_plot', 'Dowload Plot')
        ,br(),br()
        ,h5('Export Dataset')
        ,selectizeInput('filetype_dl_dataset'
          ,'Export dataset as'
          ,choices=c(
            None=''
            ,'Tab-separated values (TSV)'='tsv'
            ,'Comma-separated values (CSV)'='csv'
          )
          ,width='100%'
        )
        ,downloadButton('dl_dataset', 'Download Dataset')
        ,conditionalPanel(condition='input.do_hc_col || input.do_hc_row'
          ,br()
          ,h5('Export Distance Matrix')
          ,selectizeInput('which_dist_matrix'
            ,'Which distance matrix?'
            ,choices=''
            ,width='100%'
          )
          ,selectizeInput('filetype_dl_dist_matrix'
            ,'Export distance matrix as'
            ,choices=c(
              None=''
              ,'Tab-separated values (TSV)'='tsv'
              ,'Comma-separated values (CSV)'='csv'
            )
            ,width='100%'
          )
          ,downloadButton('dl_dist_matrix', 'Download Distance Matrix')
        )
        ,conditionalPanel(condition='input.do_hc_col || input.do_hc_row'
          ,br()
          ,h5('Export Flat Clusters')
          ,selectizeInput('which_flat_clusters'
            ,'Flat clusters of which dendrogram?'
            ,choices=''
            ,width='100%'
          )
          ,selectizeInput('filetype_dl_flat_clusters'
            ,'Export flat clusters as'
            ,choices=c(
              None=''
              ,'Tab-separated values (TSV)'='tsv'
              ,'Comma-separated values (CSV)'='csv'
            )
            ,width='100%'
          )
          ,downloadButton('dl_flat_clusters', 'Download Flat Clusters')
        )
      )
      # jQueryUI dialog $('.dialog')
      ,tags$script('
        $(document).ready(function(){
          $(".dialog").dialog({ autoOpen: false, width: "440px" });
          $(".dialog").dialog( "option", "buttons", [{
            text: "OK", 
            click: function() {
              $(this).dialog("close");
            }
          }]);
        });
        ')
      # button for dialog_load_demo_dataset
      ,HTML('<button
        type="button" 
        id="show_dialog_load_demo_dataset"
        class="btn"
        onclick=\'
          $("#dialog_load_demo_dataset").dialog("option", "position", {
              my: "left bottom",
              at: "right",
              of: $("#show_dialog_load_demo_dataset")
            });
          $("#dialog_load_demo_dataset").dialog("open");
          \'
        >
        <i class="fa fa-play">
        Load a demo dataset
        </i>
        </button>')
      # button for dialog_plot_dim
      ,br(),br()
      ,HTML('<button 
        id="show_dialog_plot_dim"
        class="btn" 
        href="javascript:void(0);" 
        onclick=\'
          $("#dialog_plot_dim").dialog("option", "position", {
              my: "left bottom",
              at: "right",
              of: $("#show_dialog_plot_dim")
            });
          $("#dialog_plot_dim").dialog("open");
          \'
        disabled
        >
        <i class="fa fa-eye">
        Change Plot Visual Parameters
        </i>
        </button>')
      # button for dialog_export_plot
      ,HTML('<button 
        id="show_dialog_export_plot"
        class="btn" 
        href="javascript:void(0);" 
        onclick=\'
          $("#dialog_export_plot").dialog("option", "position", {
              my: "left bottom",
              at: "right",
              of: $("#show_dialog_export_plot")
            });
          $("#dialog_export_plot").dialog("open");
          \'
        disabled
        >
        <i class="fa fa-save">
        Export...
        </i>
        </button>')
    )
    ,mainPanel(
      plotOutput('matrix_viz', height='100%', width='100%')
    )
  ))
  ,tabPanel('About', pageWithSidebar(
    headerPanel('')
    ,sidebarPanel(includeMarkdown('md/about_sidebar.md'))
    ,mainPanel(
      div(style='width:600px;'
        ,includeMarkdown('md/about.md')
      )
    )
  ))
))
