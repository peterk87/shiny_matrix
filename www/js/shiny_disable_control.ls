

Shiny.addCustomMessageHandler \disable_control  (data) !->
  return unless data?
  return unless data.id?
  return unless data.disable?

  el = $ "\##{data.id}"

  if el.is 'input, button'
    el.prop \disabled data.disable
  else
    el.attr \disabled data.disable



Shiny.addCustomMessageHandler \show_div  (data) !->
  return unless data?
  return unless data.id?
  return unless data.show?

  el = $ "\##{data.id}"
  el.css \display, if data.show then \block else \none