/*
Modified shinyalert.js of AnalytixWare/ShinySky by xiaodaigh
Source: https://github.com/AnalytixWare/ShinySky/blob/master/inst/www/shinyalert.js
*/

Shiny.addCustomMessageHandler \shiny_alert_handler  (data) !->
  unless data?
    return 
  container_el = $ "\##{data.id}"
  alert_el = jQuery '<div/>' class: "alert alert-dismissable alert-#{data.alert_level}" 
  if data.prepend?
    alert_el.prependTo container_el
  else
    container_el.html alert_el

  alert_el.html data.message
  
  close_button = jQuery '<button 
      type="button" 
      class="close" 
      data-dismiss="alert" 
      aria-hidden="true">
        &times;
      </button>'
    .prependTo alert_el

